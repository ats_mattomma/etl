package it.atscom.etl.service;
import java.io.IOException;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.Result;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.plugins.PluginFolder;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.core.util.EnvUtil;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;

public class EtlProcess 
{
	public static Result runTransformation(String trasformationName) throws IOException
	{
		//String percorso = "C:/Users/983733/Desktop/Transf/"; errore! directory locale!
		String db = "_ss_dest.ktr";
		try 
		{
			StepPluginType.getInstance().getPluginFolders().add(new PluginFolder("C:/Users/983733/Desktop/data-integration/plugins", false, true));
			KettleEnvironment.init();
			EnvUtil.environmentInit();
			TransMeta metaData = new TransMeta(trasformationName + db);
			Trans trans = new Trans(metaData);
			System.out.println(trans);
			trans.execute(null);
			trans.waitUntilFinished();
			if(trans.getErrors() > 0)
				throw new RuntimeException("Errors detected during the trasformation");
			Result results = trans.getResult();
			return results;
		}
		catch(KettleException e)
		{
			System.out.println(e);
			System.out.println("Check the directory of the file.");
		}
		return null;
	}
}
