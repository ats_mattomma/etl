package it.atscom.etl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EtlBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(EtlBatchApplication.class, args);
	}
}
