package it.atscom.etl.controller;

import java.io.IOException;

import org.pentaho.di.core.Result;
import org.pentaho.di.core.exception.KettleXMLException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

import it.atscom.etl.service.EtlProcess;

@RestController
public class EtlController {
	
	@RequestMapping("/")
	public ModelAndView etl ()
	{
		ModelAndView model = new ModelAndView();
		model.setViewName("etl");
		return model;
	}

	@RequestMapping(value = "/etl/{trans}", method = RequestMethod.GET)
	public String contiCorrenti(@PathVariable("trans") String transformationName) throws IOException, KettleXMLException
	{
		Result results = EtlProcess.runTransformation(transformationName);
		/*ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(results);*/
		Gson gson = new Gson();
		return gson.toJson(results);
	}
}
