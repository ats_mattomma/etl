The goal of the project is to create a web application for running trasformations (made with the tool of  PDI (Penthao Data Integration, Spoon).

The project is divided in 2 parts: a frontend in which i have used HTML, Css and Jquery technologies and a backend with Springboot.
Here the list of the technologies used and their relative versions.

The build tool used is Maven, jdk 1.8, packaging war.
HTML version: 5
Css version: 4
JQuery version: 3.4.1
Springboot version: 2.2.6.RELEASE
Penthao version: 8.3.0.0-371

The first thing in the backend to do is to create a class for running the transformation and getting the results data out of it.
To start i created a class named "EtlProcess" inside a new package named "service". In here i generated a method called "runTransformation". First you need to initialize the Kettle environment through the method "KettleEnvironment.init();", that you can find in Penthao libraries. For doing that you need to add a series of dependencies to your pom.XML.
Kettle-engine
Kettle-core
kettle-ui-swt
After having initialized the environment you have to pass to the TranMeta the directory of your transformation. I have create a folder for the transformations, and parameterized it in order to pass to the backend different trasformation, simply by just passing the name of it.
Now that we have the metaData we can pass them to the trans object and run the transformation thanks to the method tran.execute().

When the transformation is finished we can get the results with the method .getResults, always from Penthao libraries, that will will return us an object of Result type. To pass this object to the controller we have to specify that our method return type will be a Result object.
Our code is surrounded by a try catch, that will print in our console the KettleException in case the trasformation couldn't run successfully.

Our controller will be have the annotation of a @Restcontroller, since we will deal with rest API, as the specification says.
Here the requestMapping annotations will be defined. 

The first, through the use of ModelAndView object will redirect you to the main html page, where the user, in the frontend will have the opportunity to decide which transformation to run. I set the name view as "etl" in order to map our html file, called infact "etl". This requestMapping will return the ModelAndView object created.
The second requestMapping annotations, will be above a method that return a string. 

Infact, receiving from the service an object Result, it will be converted into Json. For doing that we need to add to our pom.XML a dependecy called gson, for converting objects into JSON format. We well do that with the method toJson(), of the gson object.
Moreover, in our requestMapping it's necessary to specify the a parameterized URI and the RequestMethod that will be a GET. Throught the annotations @PathVariable we can pass the parameterized name of the transformation in the URI to our method parameter. In this way the trasformation can dinamically change anytime the method is called.
Finally, the string is passed to the frontend in a Json format.

I decide to include all the code into one single page (except for the css code), an HTML file, since the very short nature of the project.
To start i created 3 links to click on, in order to lanunch the desired transformaiton. Each will redirect to the current page, so there are no other web pages involved. Each a tag will have an id associated, in order to parameterize them later.

Since i decided to use JQuery, a Javascript framwork, for the frontend, we need to add an external reference to Ajax and JQuery. We will write a function to displat the results of the transformations. This functions will involve an Ajax call. Before of that we will make use of 2 functions, AjaxStart() and AjaxComplete(), to display a loader, in the meantime that the Ajax call is performing its task.
Our Ajax call is inside a JQuery function, .click(). 

So when the user will click on the link, the call will start performing its task. The url is parameterized, in order to dinamically change, if the user click on more links. Our Ajax call will return us a string that will be converted into an object thanks to the method JSON.parse(). After that, through the use of a couple of ternary, we convert booleans value of the object into strings, more user-friendly.
Through the use of the function .getElementById("p1").innerHTML we will insert the results of the transformation into the paragraph tag.
In the end, an external css file is used to align and give some color to the webpage.